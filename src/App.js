import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Attendance from "./Attendance";

function App() {
  return (
    <div className="App">
      <Attendance title="Attendance codes for Timo's course" />
    </div>
  );
}

export default App;
